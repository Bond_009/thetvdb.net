using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using TheTVDB.Models;

namespace TheTVDB.Cached
{
    /// <summary>
    /// Class that implements <see cref="TheTVDBClient" /> with added caching.
    /// </summary>
    public class CachedTheTVDBClient : TheTVDBClient
    {
        private readonly SemaphoreSlim _cacheWriteLock = new SemaphoreSlim(1, 1);
        private readonly IMemoryCache _cache;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedTheTVDBClient"/> class.
        /// </summary>
        /// <param name="apiKey">A TheTVDB API key.</param>
        /// <param name="acceptLanguage">The desired language for responses.</param>
        /// <param name="memoryCache">The memory cache.</param>
        public CachedTheTVDBClient(
            string apiKey,
            string? acceptLanguage = null,
            IMemoryCache? memoryCache = null) : base(apiKey, acceptLanguage)
        {
            _cache = memoryCache ?? new MemoryCache(new MemoryCacheOptions());
        }

        /// <inheritdoc />
        protected override async Task<T> GetResponseAsync<T>(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var cacheKey = request.RequestUri + request.Headers.ToString();
            if (_cache.TryGetValue(cacheKey, out T cachedValue))
            {
                return cachedValue;
            }

            await _cacheWriteLock.WaitAsync(cancellationToken).ConfigureAwait(false);
            try
            {
                if (_cache.TryGetValue(cacheKey, out cachedValue))
                {
                    return cachedValue;
                }

                using var response = await HttpClient.SendAsync(
                    request,
                    HttpCompletionOption.ResponseHeadersRead,
                    cancellationToken).ConfigureAwait(false);
                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        var res = await DeserializeContentAsync<T>(response.Content, cancellationToken).ConfigureAwait(false);
                        var cacheTime = response.Headers.CacheControl?.MaxAge;
                        if (cacheTime != null)
                        {
                            _cache.Set(cacheKey, res, cacheTime.Value);
                        }

                        return res;
                    default:
                        var errRes = await DeserializeContentAsync<ErrorResponse>(response.Content, cancellationToken).ConfigureAwait(false);
                        throw response.StatusCode switch
                        {
                            HttpStatusCode.Unauthorized => new UnauthorizedAccessException(errRes.Error),
                            HttpStatusCode.NotFound => new TVDBNotFoundException(errRes.Error),
                            _ => new TVDBException(
                                    string.Format(
                                        CultureInfo.InvariantCulture,
                                        "Unexpected HTTP status code: {0}, Error: {1}",
                                        (int)response.StatusCode,
                                        errRes.Error)),
                        };
                }
            }
            finally
            {
                _cacheWriteLock.Release();
            }
        }
    }
}
