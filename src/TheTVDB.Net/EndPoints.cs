namespace TheTVDB
{
    internal static class Endpoints
    {
        public const string DefaultBaseUrl = "https://api.thetvdb.com";

        public const string Login = "/login";
        public const string RefreshToken = "/refresh_token";

        public const string Episodes = "/episodes";
        public const string Languages = "/languages";
        public const string SearchSeries = "/search/series";
        public const string SearchSeriesParams = "/search/series/params";
        public const string Series = "/series";

        public const string Banners = "/banners";
    }
}
