using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace TheTVDB.Json
{
    internal class NullableDateTimeOffsetConverter : JsonConverter<DateTimeOffset?>
    {
        /// <inheritdoc />
        public override DateTimeOffset? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            string str = reader.GetString();
            if (str.Equals("0000-00-00 00:00:00", StringComparison.Ordinal))
            {
                return null;
            }
            else
            {
                return DateTimeOffset.Parse(str);
            }
        }

        /// <inheritdoc />
        public override void Write(Utf8JsonWriter writer, DateTimeOffset? value, JsonSerializerOptions options)
        {
            if (value == null)
            {
                writer.WriteStringValue("0000-00-00 00:00:00");
            }
            else
            {
                writer.WriteStringValue(value.Value);
            }
        }
    }
}
