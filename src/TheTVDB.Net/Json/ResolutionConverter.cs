using System;
using System.Buffers;
using System.Buffers.Text;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using TheTVDB.Models;

namespace TheTVDB.Json
{
    internal class ResolutionConverter : JsonConverter<Resolution>
    {
        /// <inheritdoc />
        public override Resolution Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            ReadOnlySpan<byte> span = reader.HasValueSequence ? reader.ValueSequence.ToArray() : reader.ValueSpan;
            int index = span.BinarySearch((byte)'x');
            if (!Utf8Parser.TryParse(span.Slice(0, index), out int width, out _))
            {
                ThrowHelper.ThrowFormatException("Invalid format for resolution width.");
            }

            if (!Utf8Parser.TryParse(span.Slice(index + 1, span.Length - index - 1), out int height, out _))
            {
                ThrowHelper.ThrowFormatException("Invalid format for resolution height.");
            }

            return new Resolution(width, height);
        }

        /// <inheritdoc />
        public override void Write(Utf8JsonWriter writer, Resolution value, JsonSerializerOptions options)
        {
            Span<byte> span = new byte[32];
            int pos = 0;
            if (!Utf8Formatter.TryFormat(value.Width, span, out int bytesWritten))
            {
                ThrowHelper.ThrowFormatException(string.Empty);
            }

            pos += bytesWritten;

            span[pos++] = (byte)'x';

            if (!Utf8Formatter.TryFormat(value.Height, span.Slice(pos), out bytesWritten))
            {
                ThrowHelper.ThrowFormatException(string.Empty);
            }

            pos += bytesWritten;
            writer.WriteStringValue(span.Slice(0, pos));
        }
    }
}
