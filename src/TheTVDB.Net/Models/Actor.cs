using System;
using System.Text.Json.Serialization;
using TheTVDB.Json;

namespace TheTVDB.Models
{
    public class Actor
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("image")]
        public string Image { get; set; }

        [JsonPropertyName("imageAdded")]
        [JsonConverter(typeof(NullableDateTimeOffsetConverter))]
        public DateTimeOffset? ImageAdded { get; set; }

        [JsonPropertyName("imageAuthor")]
        public int ImageAuthor { get; set; }

        [JsonPropertyName("lastUpdated")]
        [JsonConverter(typeof(NullableDateTimeOffsetConverter))]
        public DateTimeOffset? LastUpdated { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("role")]
        public string Role { get; set; }

        [JsonPropertyName("seriesId")]
        public int SeriesId { get; set; }

        [JsonPropertyName("sortOrder")]
        public int SortOrder { get; set; }
    }
}
