using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class AuthenticationRequest
    {
        [JsonPropertyName("apikey")]
        public string Apikey { get; set; }

        [JsonPropertyName("userkey")]
        public string? Userkey { get; set; }

        [JsonPropertyName("username")]
        public string? Username { get; set; }
    }
}
