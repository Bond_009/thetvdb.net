using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class Episode
    {
        [JsonPropertyName("absoluteNumber")]
        public int? AbsoluteNumber { get; set; }

        [JsonPropertyName("airedEpisodeNumber")]
        public int AiredEpisodeNumber { get; set; }

        [JsonPropertyName("airedSeason")]
        public int? AiredSeason { get; set; }

        [JsonPropertyName("airedSeasonID")]
        public int? AiredSeasonID { get; set; }

        [JsonPropertyName("airsAfterSeason")]
        public int? AirsAfterSeason { get; set; }

        [JsonPropertyName("airsBeforeEpisode")]
        public int? AirsBeforeEpisode { get; set; }

        [JsonPropertyName("airsBeforeSeason")]
        public int? AirsBeforeSeason { get; set; }

        [Obsolete("The director key will be deprecated in favor of the new directors key in a future release.")]
        [JsonPropertyName("director")]
        public string Director { get; set; }

        [JsonPropertyName("directors")]
        public IReadOnlyList<string> Directors { get; set; }

        [JsonPropertyName("dvdChapter")]
        public int? DvdChapter { get; set; }

        [JsonPropertyName("dvdDiscid")]
        public string DvdDiscId { get; set; }

        [JsonPropertyName("dvdEpisodeNumber")]
        public int? DvdEpisode { get; set; }

        [JsonPropertyName("dvdSeason")]
        public int? DvdSeason { get; set; }

        [JsonPropertyName("episodeName")]
        public string EpisodeName { get; set; }

        [JsonPropertyName("filename")]
        public string Filename { get; set; }

        [JsonPropertyName("firstAired")]
        public string FirstAired { get; set; }

        [JsonPropertyName("guestStars")]
        public IReadOnlyList<string> GuestStars { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("imdbId")]
        public string ImdbId { get; set; }

        [JsonPropertyName("lastUpdated")]
        public int LastUpdated { get; set; }

        // The docs say that this should be a string, the docs are wrong.
        [JsonPropertyName("lastUpdatedBy")]
        public int LastUpdatedBy { get; set; }

        [JsonPropertyName("overview")]
        public string Overview { get; set; }

        [JsonPropertyName("productionCode")]
        public string ProductionCode { get; set; }

        // The docs say that this should be a string, the docs are wrong.
        [JsonPropertyName("seriesId")]
        public int SeriesId { get; set; }

        [JsonPropertyName("showUrl")]
        public string ShowUrl { get; set; }

        // REVIEW: Precision
        [JsonPropertyName("siteRating")]
        public float SiteRating { get; set; }

        [JsonPropertyName("siteRatingCount")]
        public int SiteRatingCount { get; set; }

        [JsonPropertyName("thumbAdded")]
        public string ThumbAdded { get; set; }

        [JsonPropertyName("thumbAuthor")]
        public int ThumbAuthor { get; set; }

        [JsonPropertyName("thumbHeight")]
        public string? ThumbHeight { get; set; }

        [JsonPropertyName("thumbWidth")]
        public string? ThumbWidth { get; set; }

        [JsonPropertyName("writers")]
        public IReadOnlyList<string> Writers { get; set; }
    }
}
