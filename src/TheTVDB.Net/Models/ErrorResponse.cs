using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class ErrorResponse
    {
        [JsonPropertyName("error")]
        public string Error { get; set; }
    }
}
