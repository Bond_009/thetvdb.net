using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class Errors
    {
        [JsonPropertyName("invalidFilters")]
        public IReadOnlyList<string>? InvalidFilters { get; set; }

        [JsonPropertyName("invalidLanguage")]
        public string? InvalidLanguage { get; set; }

        [JsonPropertyName("invalidQueryParams")]
        public IReadOnlyList<string>? InvalidQueryParams { get; set; }
    }
}
