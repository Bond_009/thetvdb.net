namespace TheTVDB.Models
{
    public interface IHasBanner
    {
        string Banner { get; set; }
    }

    public static class Extensions
    {
        public static string GetBannerUrl(this IHasBanner iHasBanner)
            => TheTVDBClient.GetBannerUrl(iHasBanner.Banner);
    }
}
