namespace TheTVDB.Models
{
    internal interface IHasErrors
    {
        Errors? Errors { get; set; }
    }

    internal static class IHasErrorsExtensions
    {
        public static void EnsureSuccess(this IHasErrors iHasErrors)
        {
            Errors? errors = iHasErrors.Errors;
            if (errors == null)
            {
                return;
            }

            if (errors.InvalidFilters != null)
            {
                throw new TVDBInvalidFiltersException(errors.InvalidFilters);
            }

            if (errors.InvalidLanguage != null)
            {
                throw new TVDBInvalidLanguageException(errors.InvalidLanguage);
            }

            if (errors.InvalidQueryParams != null)
            {
                throw new TVDBInvalidQueryParamsException(errors.InvalidQueryParams);
            }
        }
    }
}
