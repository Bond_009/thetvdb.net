using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class Image
    {
        [JsonPropertyName("fileName")]
        public string FileName { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("keyType")]
        public string KeyType { get; set; }

        [JsonPropertyName("languageId")]
        public int LanguageId { get; set; }

        [JsonPropertyName("ratingsinfo")]
        public RatingsInfo RatingsInfo { get; set; }

        [JsonPropertyName("resolution")]
        public Resolution Resolution { get; set; }

        [JsonPropertyName("subKey")]
        public string SubKey { get; set; }

        [JsonPropertyName("thumbnail")]
        public string Thumbnail { get; set; }
    }
}
