using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class ImageCounts
    {
        [JsonPropertyName("fanart")]
        public int FanArt { get; set; }

        [JsonPropertyName("poster")]
        public int Poster { get; set; }

        [JsonPropertyName("season")]
        public int Season { get; set; }

        [JsonPropertyName("seasonwide")]
        public int SeasonWide { get; set; }

        [JsonPropertyName("series")]
        public int Series { get; set; }
    }
}
