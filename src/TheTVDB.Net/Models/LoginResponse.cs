using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class LoginResponse
    {
        /// <summary>
        /// A JWT token for use with the rest of the API routes.
        /// </summary>
        /// <value>Returns a JWT token for use with the rest of the API routes.</value>
        [JsonPropertyName("token")]
        public string Token { get; set; }
    }
}
