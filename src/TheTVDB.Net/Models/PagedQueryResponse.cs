using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class PagedQueryResponse<T> : QueryResponse<IReadOnlyList<T>>
    {
        [JsonPropertyName("data")]
        public IReadOnlyList<T> Data { get; set; }

        [JsonPropertyName("errors")]
        public Errors? Errors { get; set; }

        [JsonPropertyName("links")]
        public Links Links { get; set; }
    }
}
