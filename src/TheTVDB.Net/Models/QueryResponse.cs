using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class QueryResponse<T> : Response<T>, IHasErrors
    {
        [JsonPropertyName("errors")]
        public Errors? Errors { get; set; }
    }
}
