using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class RatingsInfo
    {
        [JsonPropertyName("average")]
        public int Average { get; set; }

        [JsonPropertyName("Count")]
        public int count { get; set; }
    }
}
