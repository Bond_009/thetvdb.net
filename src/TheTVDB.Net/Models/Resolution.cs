namespace TheTVDB.Models
{
    public struct Resolution
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Resolution"/> struct.
        /// </summary>
        /// <param name="width">The Width of the image.</param>
        /// <param name="height">The height of the image.</param>
        public Resolution(int width, int height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Gets or sets the Width of the image.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the image.
        /// </summary>
        public int Height { get; set; }
    }
}
