using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    internal class Response<T>
    {
        [JsonPropertyName("data")]
        public T Data { get; set; }
    }
}
