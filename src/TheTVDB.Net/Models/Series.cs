using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class Series : IHasBanner
    {
        [JsonPropertyName("added")]
        public string Added { get; set; }

        [JsonPropertyName("airsDayOfWeek")]
        public string AirsDayOfWeek { get; set; }

        [JsonPropertyName("airsTime")]
        public string AirsTime { get; set; }

        [JsonPropertyName("aliases")]
        public IReadOnlyList<string> Aliases { get; set; }

        [JsonPropertyName("banner")]
        public string Banner { get; set; }

        [JsonPropertyName("firstAired")]
        public string FirstAired { get; set; }

        [JsonPropertyName("genre")]
        public IReadOnlyList<string> Genre { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("imdbId")]
        public string ImdbId { get; set; }

        [JsonPropertyName("lastUpdated")]
        public int LastUpdated { get; set; }

        [JsonPropertyName("network")]
        public string Network { get; set; }

        [JsonPropertyName("networkId")]
        public string NetworkId { get; set; }

        [JsonPropertyName("overview")]
        public string Overview { get; set; }

        [JsonPropertyName("rating")]
        public string Rating { get; set; }

        [JsonPropertyName("runtime")]
        public string Runtime { get; set; }

        [JsonPropertyName("seriesId")]
        public string SeriesId { get; set; }

        [JsonPropertyName("seriesName")]
        public string SeriesName { get; set; }

        [JsonPropertyName("siteRating")]
        public float SiteRating { get; set; }

        [JsonPropertyName("siteRatingCount")]
        public int SiteRatingCount { get; set; }

        [JsonPropertyName("slug")]
        public string Slug { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("zap2itId")]
        public string Zap2itId { get; set; }
    }
}
