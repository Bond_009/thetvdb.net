using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class SeriesSearchResult : IHasBanner
    {
        [JsonPropertyName("aliases")]
        public IReadOnlyList<string> Aliases { get; set; }

        [JsonPropertyName("banner")]
        public string Banner { get; set; }

        [JsonPropertyName("firstAired")]
        public string FirstAired { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("network")]
        public string Network { get; set; }

        [JsonPropertyName("overview")]
        public string Overview { get; set; }

        [JsonPropertyName("seriesName")]
        public string SeriesName { get; set; }

        [JsonPropertyName("slug")]
        public string Slug { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}
