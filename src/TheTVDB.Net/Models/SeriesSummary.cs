using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TheTVDB.Models
{
    public class SeriesSummary
    {
        [JsonPropertyName("airedEpisodes")]
        public string AiredEpisodes { get; set; }

        [JsonPropertyName("airedSeasons")]
        public IReadOnlyList<string> AiredSeasons { get; set; }

        [JsonPropertyName("dvdEpisodes")]
        public string DvdEpisodes { get; set; }

        [JsonPropertyName("dvdSeasons")]
        public IReadOnlyList<string> DvdSeasons { get; set; }
    }
}
