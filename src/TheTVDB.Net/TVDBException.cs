using System;

namespace TheTVDB
{
    /// <summary>
    /// The exception that is thrown when the TheTVDB API returns an 404 or unexpected status code.
    /// </summary>
    public class TVDBException : Exception
    {
        /// <inheritdoc />
        internal TVDBException() : base()
        {
        }

        /// <inheritdoc />
        internal TVDBException(string message) : base(message)
        {
        }

        /// <inheritdoc />
        internal TVDBException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
