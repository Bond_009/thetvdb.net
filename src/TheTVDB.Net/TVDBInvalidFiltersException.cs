using System.Collections.Generic;

namespace TheTVDB
{
    /// <summary>
    /// The exception that is thrown when the TheTVDB API returns an invalid filters error.
    /// </summary>
    public class TVDBInvalidFiltersException : TVDBException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TVDBInvalidFiltersException"/> class.
        /// </summary>
        /// <param name="filters">The filters that caused the exception.</param>
        internal TVDBInvalidFiltersException(IReadOnlyList<string> filters) : base()
        {
            Filters = filters;
        }

        /// <summary>
        /// Gets the filters that caused the exception.
        /// </summary>
        public IReadOnlyList<string> Filters { get; }
    }
}
