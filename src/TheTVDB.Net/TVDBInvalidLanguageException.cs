namespace TheTVDB
{
    /// <summary>
    /// The exception that is thrown when the TheTVDB API returns an invalid language error.
    /// </summary>
    public class TVDBInvalidLanguageException : TVDBException
    {
        /// <inheritdoc />
        internal TVDBInvalidLanguageException(string message) : base(message)
        {
        }
    }
}
