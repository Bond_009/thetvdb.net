using System.Collections.Generic;

namespace TheTVDB
{
    /// <summary>
    /// The exception that is thrown when the TheTVDB API returns an invalid query parameters error.
    /// </summary>
    public class TVDBInvalidQueryParamsException : TVDBException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TVDBInvalidQueryParamsException"/> class.
        /// </summary>
        /// <param name="queryParams">The query parameters that caused the exception.</param>
        internal TVDBInvalidQueryParamsException(IReadOnlyList<string> queryParams) : base()
        {
            QueryParams = queryParams;
        }

        /// <summary>
        /// Gets the query parameters that caused the exception.
        /// </summary>
        public IReadOnlyList<string> QueryParams { get; }
    }
}
