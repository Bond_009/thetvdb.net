namespace TheTVDB
{
    /// <summary>
    /// The exception that is thrown when the TheTVDB API returns an 404 status code.
    /// </summary>
    public class TVDBNotFoundException : TVDBException
    {
        /// <inheritdoc />
        internal TVDBNotFoundException(string message)
            : base(message)
        {
        }
    }
}
