﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using TheTVDB.Models;
using TheTVDB.Json;

namespace TheTVDB
{
    public class TheTVDBClient : IDisposable
    {
        private readonly JsonSerializerOptions _jsonOptions;
        private readonly HttpClient _httpClient;
        private bool _disposed = false;

        private string? _token;
        private DateTimeOffset _lastTokenRefresh;
        private TimeSpan _refreshWindow;
        private string _apiKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="TheTVDBClient"/> class.
        /// </summary>
        /// <param name="apiKey">A TheTVDB API key.</param>
        /// <param name="acceptLanguage">The desired language for responses.</param>
        public TheTVDBClient(string apiKey, string? acceptLanguage = null)
        {
            if (apiKey.Length == 0)
            {
                ThrowHelper.ThrowArgumentException("String can't be empty.", nameof(apiKey));
            }

            _jsonOptions = new JsonSerializerOptions()
            {
                IgnoreNullValues = true
            };
            _jsonOptions.Converters.Add(new ResolutionConverter());

            _httpClient = new HttpClient()
            {
                BaseAddress = new Uri(Endpoints.DefaultBaseUrl)
            };

            _apiKey = apiKey;
            AcceptLanguage = acceptLanguage;
        }

        /// <summary>
        /// Gets the http client.
        /// </summary>
        protected HttpClient HttpClient => _httpClient;

        /// <summary>
        /// Gets or sets the desired language for responses.
        /// </summary>
        public string? AcceptLanguage { get; set; }

        /// <summary>
        /// Gets or sets the <c>BaseAddress</c> used for requests.
        /// </summary>
        public Uri BaseAddress
        {
            get => _httpClient.BaseAddress;
            set => _httpClient.BaseAddress = value;
        }

        public async Task<Episode> GetEpisodeAsync(
            int id,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            // REVIEW: Find the most efficient way to construct the url
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Episodes).AppendPathSegment(id.ToString(CultureInfo.InvariantCulture)).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<QueryResponse<Episode>>(reqMsg, cancellationToken).ConfigureAwait(false);
            response.EnsureSuccess();
            return response.Data;
        }

        public async Task<IReadOnlyList<Language>> GetLanguagesAsync(CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(HttpMethod.Get, Endpoints.Languages);

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            var response = await GetResponseAsync<ObjectArray<Language>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<Language> GetLanguageAsync(int id, CancellationToken cancellationToken = default)
        {
            // REVIEW: Find the most efficient way to construct the url
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Languages).AppendPathSegment(id.ToString(CultureInfo.InvariantCulture)).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            return await GetResponseAsync<Language>(reqMsg, cancellationToken).ConfigureAwait(false);
        }

        public async Task<IReadOnlyList<SeriesSearchResult>> GetSeriesByNameAsync(
            string name,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.SearchSeries).AddParameter("name", name).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<ObjectArray<SeriesSearchResult>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<IReadOnlyList<SeriesSearchResult>> GetSeriesByIMBdIdAsync(
            string imdbId,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.SearchSeries).AddParameter("imdbId", imdbId).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<ObjectArray<SeriesSearchResult>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<IReadOnlyList<SeriesSearchResult>> GetSeriesByZap2itIdAsync(
            string zap2itId,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.SearchSeries).AddParameter("zap2itId", zap2itId).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<ObjectArray<SeriesSearchResult>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<IReadOnlyList<SeriesSearchResult>> GetSeriesBySlugAsync(
            string slug,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.SearchSeries).AddParameter("slug", slug).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<ObjectArray<SeriesSearchResult>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public async Task<Series> GetSeriesAsync(
            int id,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            // REVIEW: Find the most efficient way to construct the url
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Series).AppendPathSegment(id.ToString(CultureInfo.InvariantCulture)).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<QueryResponse<Series>>(reqMsg, cancellationToken).ConfigureAwait(false);
            response.EnsureSuccess();
            return response.Data;
        }

        public async Task<IReadOnlyList<Actor>> GetActorsAsync(int seriesId, CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Series)
                    .AppendPathSegment(seriesId.ToString(CultureInfo.InvariantCulture))
                    .AppendPathSegment("actors").ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            var response = await GetResponseAsync<QueryResponse<IReadOnlyList<Actor>>>(reqMsg, cancellationToken).ConfigureAwait(false);
            response.EnsureSuccess();
            return response.Data;
        }

        public async IAsyncEnumerable<IReadOnlyList<Episode>> GetEpisodesAsync(
            int seriesId,
            int? absoluteNumber = null,
            int? airedSeason = null,
            int? dvdSeason = null,
            int? dvdEpisode = null,
            int? imdbId = null,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            int page = 1;
            PagedQueryResponse<Episode> response;
            do
            {
                using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                BuildEpisodesQuery(
                    seriesId,
                    page,
                    absoluteNumber,
                    airedSeason,
                    dvdSeason,
                    dvdEpisode,
                    imdbId).ToString());

                await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

                AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

                response = await GetResponseAsync<PagedQueryResponse<Episode>>(reqMsg, cancellationToken).ConfigureAwait(false);
                response.EnsureSuccess();
                yield return response.Data;
            } while (response.Links.Next == ++page);
        }

        public async Task<IReadOnlyList<Episode>> GetEpisodesAsync(
            int seriesId,
            int page,
            int? absoluteNumber = null,
            int? airedSeason = null,
            int? dvdSeason = null,
            int? dvdEpisode = null,
            int? imdbId = null,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            if (page < 1)
            {
                ThrowHelper.ThrowArgumentOutOfRangeException("Value can't be lower than one", nameof(page));
            }

            using var reqMsg = new HttpRequestMessage(
            HttpMethod.Get,
            BuildEpisodesQuery(
                seriesId,
                page,
                absoluteNumber,
                airedSeason,
                dvdSeason,
                dvdEpisode,
                imdbId).ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<PagedQueryResponse<Episode>>(reqMsg, cancellationToken).ConfigureAwait(false);
            response.EnsureSuccess();
            return response.Data;
        }

        private UrlBuilder BuildEpisodesQuery(
            int seriesId,
            int page,
            int? absoluteNumber,
            int? airedSeason,
            int? dvdSeason,
            int? dvdEpisode,
            int? imdbId)
        {
            var url = new UrlBuilder(Endpoints.Series)
                .AppendPathSegment(seriesId.ToString(CultureInfo.InvariantCulture))
                .AppendPathSegment("episodes/query")
                .AddParameter("page", page);

            if (absoluteNumber.HasValue)
            {
                url.AddParameter("absoluteNumber", absoluteNumber.Value);
            }

            if (airedSeason.HasValue)
            {
                url.AddParameter("airedSeason", airedSeason.Value);
            }

            if (dvdSeason.HasValue)
            {
                url.AddParameter("dvdSeason", dvdSeason.Value);
            }

            if (dvdEpisode.HasValue)
            {
                url.AddParameter("dvdEpisode", dvdEpisode.Value);
            }

            if (imdbId.HasValue)
            {
                url.AddParameter("imdbId", imdbId.Value);
            }

            return url;
        }

        /// <summary>
        /// Returns a summary of the episodes and seasons available for the series.
        /// Note: Season "0" is for all episodes that are considered to be specials.
        /// </summary>
        /// <param name="seriesId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>A summary of the episodes and seasons available for the series.</returns>
        public async Task<SeriesSummary> GetSeriesSummaryAsync(
            int seriesId,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Series)
                    .AppendPathSegment(seriesId.ToString(CultureInfo.InvariantCulture))
                    .AppendPathSegment("episodes/summary").ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            var response = await GetResponseAsync<QueryResponse<SeriesSummary>>(reqMsg, cancellationToken).ConfigureAwait(false);
            response.EnsureSuccess();
            return response.Data;
        }

        /// <summary>
        /// Returns a summary of the images for a particular series.
        /// </summary>
        /// <param name="seriesId">Id of the series.</param>
        /// <param name="acceptLanguageOverride"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>A summary of the images for a particular series.</returns>
        public async Task<ImageCounts> GetSeriesImagesAsync(
            int seriesId,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Series)
                    .AppendPathSegment(seriesId.ToString(CultureInfo.InvariantCulture))
                    .AppendPathSegment("images").ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<Response<ImageCounts>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        /// <summary>
        /// Query images for the given series Id.
        /// </summary>
        /// <param name="seriesId">Id of the series.</param>
        /// <param name="keyType"></param>
        /// <param name="resolution"></param>
        /// <param name="subKey"></param>
        /// <param name="acceptLanguageOverride"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Image>> GetSeriesImagesAsync(
            int seriesId,
            string? keyType = null,
            Resolution? resolution = null,
            string? subKey = null,
            string? acceptLanguageOverride = null,
            CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(
                HttpMethod.Get,
                new UrlBuilder(Endpoints.Series)
                    .AppendPathSegment(seriesId.ToString(CultureInfo.InvariantCulture))
                    .AppendPathSegment("images/query").ToString());

            await EnsureTokenAsync(reqMsg.Headers, cancellationToken).ConfigureAwait(false);

            AddAcceptLanguage(reqMsg.Headers, acceptLanguageOverride);

            var response = await GetResponseAsync<QueryResponse<IReadOnlyList<Image>>>(reqMsg, cancellationToken).ConfigureAwait(false);
            return response.Data;
        }

        public static string GetBannerUrl(string banner)
            => Endpoints.Banners + banner;

        private async Task EnsureTokenAsync(HttpRequestHeaders reqHeaders, CancellationToken cancellationToken)
        {
            if (_token == null || DateTimeOffset.Now - _lastTokenRefresh > TimeSpan.FromDays(24))
            {
                await LoginAsync(cancellationToken).ConfigureAwait(false);
            }
            else if (TimeSpan.FromDays(24) - (DateTime.Now - _lastTokenRefresh) < _refreshWindow)
            {
                await RefreshTokenAsync(cancellationToken).ConfigureAwait(false);
            }

            reqHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
        }

        private async Task LoginAsync(CancellationToken cancellationToken = default)
        {
            // TODO: @bond Add support for user auth.
            var authReq = new AuthenticationRequest()
            {
                Apikey = _apiKey
            };

            using var reqMsg = new HttpRequestMessage(HttpMethod.Get, Endpoints.Login)
            {
                Content = new ByteArrayContent(
                    JsonSerializer.SerializeToUtf8Bytes(authReq, _jsonOptions))
            };

            reqMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await GetResponseAsync<LoginResponse>(reqMsg, cancellationToken).ConfigureAwait(false);
            _token = response.Token;
            _lastTokenRefresh = DateTimeOffset.Now;
        }

        private async Task RefreshTokenAsync(CancellationToken cancellationToken = default)
        {
            using var reqMsg = new HttpRequestMessage(HttpMethod.Get, Endpoints.RefreshToken);
            var response = await GetResponseAsync<LoginResponse>(reqMsg, cancellationToken).ConfigureAwait(false);
            _token = response.Token;
            _lastTokenRefresh = DateTimeOffset.Now;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AddAcceptLanguage(HttpRequestHeaders headers, string? acceptLanguageOverride)
        {
            if (acceptLanguageOverride != null)
            {
                headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(acceptLanguageOverride));
                return;
            }

            if (AcceptLanguage != null)
            {
                headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(AcceptLanguage));
            }
        }

        protected virtual async Task<T> GetResponseAsync<T>(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            using var response = await _httpClient.SendAsync(
                request,
                HttpCompletionOption.ResponseHeadersRead,
                cancellationToken).ConfigureAwait(false);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    return await DeserializeContentAsync<T>(response.Content, cancellationToken).ConfigureAwait(false);
                default:
                    var errRes = await DeserializeContentAsync<ErrorResponse>(response.Content, cancellationToken).ConfigureAwait(false);
                    throw response.StatusCode switch
                    {
                        HttpStatusCode.Unauthorized => new UnauthorizedAccessException(errRes.Error),
                        HttpStatusCode.NotFound => new TVDBNotFoundException(errRes.Error),
                        _ => new TVDBException(
                                string.Format(
                                    CultureInfo.InvariantCulture,
                                    "Unexpected HTTP status code: {0}, Error: {1}",
                                    (int)response.StatusCode,
                                    errRes.Error)),
                    };
            }
        }

        protected async Task<T> DeserializeContentAsync<T>(HttpContent content, CancellationToken cancellationToken)
            => await JsonSerializer.DeserializeAsync<T>(
                await content.ReadAsStreamAsync().ConfigureAwait(false),
                _jsonOptions,
                cancellationToken).ConfigureAwait(false);

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="disposing">Whether or not the managed resources should be disposed.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _httpClient.Dispose();
            }

            _disposed = true;
        }
    }
}
