using System;
using System.Collections.Generic;

namespace TheTVDB
{
    /// <summary>
    /// Provides methods for throwing common exceptions.
    /// </summary>
    internal static class ThrowHelper
    {
        /// <summary>
        /// Throws a new <see cref="ArgumentException" /> with the specified parameters.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        public static void ThrowArgumentException(string message, string paramName)
        {
            throw new ArgumentException(message, paramName);
        }

        /// <summary>
        /// Throws a new <see cref="ArgumentOutOfRangeException" /> with the specified parameters.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused the current exception.</param>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public static void ThrowArgumentOutOfRangeException(string paramName, string message)
        {
            throw new ArgumentOutOfRangeException(paramName, message);
        }

        /// <summary>
        /// Throws a new <see cref="FormatException" /> with the specified parameters.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public static void ThrowFormatException(string message)
        {
            throw new FormatException(message);
        }

        /// <summary>
        /// Throws a new <see cref="TVDBInvalidFiltersException" /> with the specified parameters.
        /// </summary>
        /// <param name="filters">The filters that caused the exception.</param>
        public static void ThrowTVDBInvalidFiltersException(IReadOnlyList<string> filters)
        {
            throw new TVDBInvalidFiltersException(filters);
        }

        /// <summary>
        /// Throws a new <see cref="TVDBInvalidLanguageException" /> with the specified parameters.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public static void ThrowTVDBInvalidLanguageException(string message)
        {
            throw new TVDBInvalidLanguageException(message);
        }

        /// <summary>
        /// Throws a new <see cref="TVDBInvalidFiltersException" /> with the specified parameters.
        /// </summary>
        /// <param name="filters">The quert parameters that caused the exception.</param>
        public static void TVDBInvalidQueryParamsException(IReadOnlyList<string> filters)
        {
            throw new TVDBInvalidQueryParamsException(filters);
        }
    }
}
