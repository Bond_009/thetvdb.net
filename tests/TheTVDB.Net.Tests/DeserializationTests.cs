using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;
using TheTVDB.Models;

namespace TheTVDB.Net.Tests
{
    public class DeserializationTests
    {
        public readonly JsonSerializerOptions _jsonOptions = new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = false
        };

        [Theory]
        [InlineData(0)]
        [InlineData(80379)]
        [InlineData(332484)]
        [InlineData(7177179)]
        // Can't return a ValueTask, XUnit won't notice the test failing
        public async Task<Episode> DeserializeEpisodeTest(int id)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/episode_{0}.json",
                    id));
            var res = await JsonSerializer.DeserializeAsync<QueryResponse<Episode>>(stream, _jsonOptions);
            Assert.Equal(id, res.Data.Id);
            return res.Data;
        }

        [Fact]
        public async Task<IReadOnlyList<Language>> DeserializeLanguages()
        {
            using FileStream stream = File.OpenRead("./Data/languages.json");
            var res = await JsonSerializer.DeserializeAsync<ObjectArray<Language>>(stream, _jsonOptions);
            return res.Data;
        }

        [Theory]
        [InlineData(7)]
        public async Task<Language> DeserializeLanguageTest(int id)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/language_{0}.json",
                    id));
            var res = await JsonSerializer.DeserializeAsync<QueryResponse<Language>>(stream, _jsonOptions);
            Assert.Equal(id, res.Data.Id);
            return res.Data;
        }

        [Theory]
        [InlineData("the-big-bang-theory")]
        public async Task<IReadOnlyList<SeriesSearchResult>> DeserializeSearchSeriesTest(string slug)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/search_series_{0}.json",
                    slug));
            var res = await JsonSerializer.DeserializeAsync<ObjectArray<SeriesSearchResult>>(stream, _jsonOptions);
            Assert.Equal(slug, res.Data[0].Slug);
            return res.Data;
        }

        [Theory]
        [InlineData(80379)]
        public async Task<Series> DeserializeSeriesTest(int id)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/series_{0}.json",
                    id));
            var res = await JsonSerializer.DeserializeAsync<QueryResponse<Series>>(stream, _jsonOptions);
            Assert.Equal(id, res.Data.Id);
            return res.Data;
        }

        [Theory]
        [InlineData(80379)]
        public async Task<IReadOnlyList<Actor>> DeserializeActorsTest(int seriesId)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/series_actors_{0}.json",
                    seriesId));
            var res = await JsonSerializer.DeserializeAsync<QueryResponse<IReadOnlyList<Actor>>>(stream, _jsonOptions);
            Assert.Equal(seriesId, res.Data[0].SeriesId);
            return res.Data;
        }

        [Theory]
        [InlineData(80379)]
        public async Task<SeriesSummary> DeserializeSeriesSummaryTest(int seriesId)
        {
            using FileStream stream = File.OpenRead(
                string.Format(
                    CultureInfo.InvariantCulture,
                    "./Data/series_summary_{0}.json",
                    seriesId));
            var res = await JsonSerializer.DeserializeAsync<QueryResponse<SeriesSummary>>(stream, _jsonOptions);
            return res.Data;
        }
    }
}
