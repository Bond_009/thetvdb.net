using System.Text.Json;
using System.Text.Json.Serialization;
using Xunit;
using TheTVDB.Models;
using TheTVDB.Json;

namespace TheTVDB.Net.Tests
{
    public class ResolutionConverterTests
    {
        public readonly JsonSerializerOptions _jsonOptions;

        public ResolutionConverterTests()
        {
            _jsonOptions = new JsonSerializerOptions();
            _jsonOptions.Converters.Add(new ResolutionConverter());
        }

        private struct TestModel
        {
            [JsonPropertyName("resolution")]
            public Resolution Resolution { get; set; }
        }

        [Theory]
        [InlineData("{\"resolution\":\"1280x1024\"}", 1280, 1024)]
        public void DeserializeResolutionTest(string json, int width, int heigth)
        {
            var res = JsonSerializer.Deserialize<TestModel>(json, _jsonOptions);
            Assert.Equal(width, res.Resolution.Width);
            Assert.Equal(heigth, res.Resolution.Height);
        }

        [Theory]
        [InlineData("{\"resolution\":\"1280x1024\"}")]
        public void ResolutionRoundtripTest(string json)
        {
            var res = JsonSerializer.Deserialize<TestModel>(json, _jsonOptions);
            Assert.Equal(json, JsonSerializer.Serialize(res, _jsonOptions));
        }
    }
}
